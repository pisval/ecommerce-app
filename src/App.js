import { commerce } from './lib/commerce'
import { useState, useEffect } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Products from './components/Products'
import NavBar from './components/NavBar';
import Footer from './components/Footer/Footer';



function App() {
  //This is my first state
  const [ products, setProducts] = useState([]);
  const [basketData, setBasketData] = useState({});
  
  // This code will fetch all the product from my API
  const fetchProducts = async () => {
    const response = await commerce.products.list();
    setProducts((response && response.data) || []);
    //console.log(response)
  };
  


  useEffect(() =>{
      fetchProducts();
  }, [])


    return (  
          <Router>
              <div>
                  <NavBar
                      basketItems={basketData.total_items}
                      totalCost={
                        (basketData.subtotal &&
                          basketData.subtotal.formatted_with_symbol) ||
                        "00.00"
                    }
                    />
                <Switch>
                  <Route exact path='/'>
                     <Products products={products}/>
                  </Route>
                </Switch>
              </div>
              <Footer/>
           </Router>
  );
}

export default App;
