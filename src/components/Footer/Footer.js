import "./style.css";
import {
  Box,
  Container,
  Row,
  Column,
  FooterLink,
  Heading,
  rowTop,
} from "./FooterStyles";
  

const Footer = () => {
  const date = new Date();
  const fullYear = date.getFullYear();
  return (
    <footer className="footer">
      <Row className="hello__world">
          <Column>
          <img className="visa_id" src="https://financialit.net/sites/default/files/2000px-visa_inc._logo.svg_.png" alt="" />
          </Column>

          <Column>
              <img className="visa_id"  src="https://i.dlpng.com/static/png/6756548_preview.png" alt="" />
          </Column>

          <Column>
              <img className="visa_id"  src="https://upload.wikimedia.org/wikipedia/en/e/eb/Stripe_logo%2C_revised_2016.png" alt="" />
          </Column>

          <Column>
              <img className="visa_id"  src="https://upload.wikimedia.org/wikipedia/en/9/99/PayU_Corporate_Logo.png" alt="" />
          </Column>

          <Column>
              <img className="visa_id"  src="https://upload.wikimedia.org/wikipedia/en/thumb/e/e7/Payoneer_logo.svg/1280px-Payoneer_logo.svg.png" alt="" />
          </Column>

          <Column>
              <img className="visa_id"  src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b7/MasterCard_Logo.svg/2560px-MasterCard_Logo.svg.png" alt="" />
          </Column>
       </Row>
        
    <Box>
    <h1 style={{ color: "green", 
                 textAlign: "center", 
                 marginTop: "-50px" }}>
    </h1>
    <Container>
      <Row>
        <Column>
          <Heading>About Us</Heading>
          <FooterLink href="#">About pisvaltech.com</FooterLink>
          <FooterLink href="#">About Pisval Tech Group</FooterLink>
          <FooterLink href="#">Sitemap</FooterLink>
        </Column>
        <Column>
          <Heading>Customer service</Heading>
          <FooterLink href="#">Customer service</FooterLink>
          <FooterLink href="#">Transaction Services Agreement</FooterLink>
          <FooterLink href="#">Take our feedback survey</FooterLink>
        </Column>
        <Column>
          <Heading>Help center</Heading>
          <FooterLink href="#">Making paymentsh</FooterLink>
          <FooterLink href="#">Delivery options</FooterLink>
          <FooterLink href="#">Buyer Protection</FooterLink>
        </Column>
        <Column>
          <Heading>Trade Services</Heading>
          <FooterLink href="#">
            <i className="fab fa-facebook-f">
              <span style={{ marginLeft: "10px" }}>
                Facebook
              </span>
            </i>
          </FooterLink>
          <FooterLink href="#">
            <i className="fab fa-instagram">
              <span style={{ marginLeft: "10px" }}>
                Instagram
              </span>
            </i>
          </FooterLink>
          <FooterLink href="#">
            <i className="fab fa-twitter">
              <span style={{ marginLeft: "10px" }}>
                Twitter
              </span>
            </i>
          </FooterLink>
          <FooterLink href="#">
            <i className="fab fa-youtube">
              <span style={{ marginLeft: "10px" }}>
                Youtube
              </span>
            </i>
          </FooterLink>
        </Column>
      </Row>
    </Container>
    <p>
    All &copy; copy rights are reserved to Pisval Tech {fullYear}
  </p>
  </Box>
      
    </footer>
  );
};

export default Footer;
