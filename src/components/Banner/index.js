import { Container, Typography, Button, Grid } from "@material-ui/core";
import logo from "./Canon-Kit.png";
import logos from "./sony-tv.png";
import "./style.css";

const Banner = () => {
  return (
    <div className="banner">
      <Container>
        <Grid className="brand" item sm={6}>
           <img src={logos} alt="Brand-tv" />
        </Grid>
        <Grid container spacing={4}>
          <Grid item xs={12} sm={6}>
            <Typography className="title" variant="h1">
              Buy, Shop and Enjoy With Us!
            </Typography>
            <Button className="shopping-button" href="#products">
              Shopping
            </Button>
          </Grid>
          <Grid className="brand" item sm={6}>
            <img src={logo} alt="Brand-tv" />
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default Banner;
