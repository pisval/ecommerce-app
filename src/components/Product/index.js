import React from 'react'
import CustomCard from '../CustomCard'
import './style.css'

const Product = ({ basket, product, addProduct, RemoveItemFromBasket }) => {
    return (
        <>
            <CustomCard
                    basket={basket}
                    product={product}
                    addProduct={addProduct}
                    RemoveItemFromBasket={RemoveItemFromBasket}
            />
        </>
    )
}

export default Product
